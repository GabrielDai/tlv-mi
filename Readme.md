# TLV-mi

It's a small communication middleware, i.e. it contains template classes which
help to encode(decode) from(to) TLV into(from) transmission(reception) buffers
according to a user defined protocol.

## Main features

- Written in C++14
- Adaptable for devices with low resources, e.g. IoT
- Low overhead while calling function API for sending (see TLVManager)
- Transmission queues with priority (see BuffersManager)
- Python implementation for easy prototyping (this is not attempt to be used on
an embedded system)

## How to use it

If you want to see an example of use please visit
[3Wheelsbot](https://gitlab.com/GabrielDai/3wheelsbot/-/tree/master).

The user needs to implement the following things:
- A protocol for sending the TLVs [(see example)](https://gitlab.com/GabrielDai/3wheelsbot/-/blob/master/comm/inc/comm/SmallProtocol.h)
- The function send() [(see example)](https://gitlab.com/GabrielDai/3wheelsbot/-/blob/master/comm/inc/comm/Send.h)
- Define an enum for Type [(see example)](https://gitlab.com/GabrielDai/3wheelsbot/-/blob/master/comm/inc/comm/CommProperties.h)
