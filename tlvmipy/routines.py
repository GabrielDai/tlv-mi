#!python3
#
# SPDX-FileCopyrightText: 2020 Gabriel Moyano <vgmoyano@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from .managers import BuffersManager
from .managers import TLV


class Encode:
    """Extract data from TLVManager, copy them into BuffersManager and then
    call the consumer.

    The consumer needs to be implemented by the user. The user is who decide
    how the bytes are consumed (e.g. writing them into a file, sending them
    through a serial port or a socket).

    Similar to C++ class TLVMi::EncodeRoutine.
    """

    def __init__(self, tlv_manager, protocol, consumer):
        self.tlv_manager = tlv_manager
        self.protocol = protocol
        self.consumer = consumer
        self.buffers_manager = BuffersManager(len(tlv_manager.jobs_lists))

    def __call__(self):
        self.run()

    def run(self):
        i = 0
        while i < len(self.tlv_manager.jobs_lists):
            tlv = self.tlv_manager.get_pending_tlv(i)
            if tlv != None:
                self.protocol.encode(
                        lambda byte: self.buffers_manager.push_byte(i, byte),
                        tlv)
            else:
                i += 1

        self.consumer.deliver_bytes(self.buffers_manager)


class MsgVerifier:
    """Auxiliary class used by DecodeRoutine to know, if all bytes of a TLV
    have been received.

    Similar to C++ class TLVMi::MsgVerifier.
    """

    def __init__(self, types):
        self.waiting_msgs = [0 for i in range(types)]

    def register_complete_msg(self, msg_type):
        if len(self.waiting_msgs) > msg_type:
            self.waiting_msgs[msg_type] += 1

    def some_complete_msg(self):
        any_received_msg = False

        for i in range(len(self.waiting_msgs)):
            if self.waiting_msgs[i] > 0:
                any_received_msg = True
                self.buffer_with_msg = i
                break

        return any_received_msg

    def get_buffer_with_complete_msg(self):
        self.waiting_msgs[self.buffer_with_msg] -= 1
        return self.buffer_with_msg


class Decode:
    """Receive data, decode it into BuffersManager and if all bytes have been
    received create a TLV, which is pushed into TLVManager.

    The user should implement the data receiver because this depends on how
    the data is sent.

    The created TLVManager can be dispatched by TLVDispatcher.

    Similar to C++ class TLVMi::DecodeRoutine.
    """

    def __init__(self, tlv_manager, protocol, receiver):
        self.tlv_manager = tlv_manager
        self.protocol_decoder = protocol.Decoder()
        self.receiver = receiver
        self.buffers_manager = BuffersManager(len(tlv_manager.jobs_lists))
        self.msg_verifier = MsgVerifier(len(tlv_manager.jobs_lists))

    def __call__(self):
        self.run()

    def run(self):
        rx_bytes = self.receiver.obtain_bytes()
        if rx_bytes:
            self.protocol_decoder.decode(
                    rx_bytes,
                    lambda i, byte: self.buffers_manager.push_byte(i, byte),
                    lambda i: self.msg_verifier.register_complete_msg(i))

            while self.msg_verifier.some_complete_msg():
                msg_buffer = self.msg_verifier.get_buffer_with_complete_msg()
                self.create_tlv(msg_buffer)

    def create_tlv(self, tlv_type):
        length = self.buffers_manager.get_byte(tlv_type)
        value = bytearray()
        for i in range(length):
            byte = self.buffers_manager.get_byte(tlv_type)
            if byte != None:
                value.append(byte)
        tlv = TLV(tlv_type, length, value)

        if not self.tlv_manager.push_tlv(tlv):
            print("[Decoding] Producing data faster than consuming\r")
