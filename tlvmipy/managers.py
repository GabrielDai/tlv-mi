#!python3
#
# SPDX-FileCopyrightText: 2020 Gabriel Moyano <vgmoyano@gmail.com>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

from threading import Lock


class TLV:
    """Generic TLV(Type Length Value) definition.

    Similar to C++ class TLVMi::TLV.
    """

    def __init__(self, tlv_type, length, value):
        self.tlv_type = tlv_type
        self.length = length
        self.value = value


class TLVManager:
    """It contains buffers for all desired TLV's types and controls the access to them.
    An unprocessed TLV is called also as pending job.

    Similar to C++ class TLVMi::TLVManager but it doesn't have exact the same
    behaviour.
    """

    def __init__(self, types):
        self.jobs_lists = [[] for i in range(types)]
        self.mutex = Lock()

    def push_tlv(self, tlv):
        job_registred = False

        self.mutex.acquire()
        if len(self.jobs_lists) > tlv.tlv_type:
            self.jobs_lists[tlv.tlv_type].append(tlv)
            job_registred = True
        self.mutex.release()

        return job_registred

    def get_pending_tlv(self, index):
        pending_job = None

        self.mutex.acquire()
        if len(self.jobs_lists) > index:
            if self.jobs_lists[index]:
                pending_job = self.jobs_lists[index].pop(0)
        self.mutex.release()

        return pending_job


class BuffersManager:
    """It contains buffers and controls access to them. These buffers are used to
    save the representations in bytes of encoded types according to a protocol.
    User must define this protocol.

    Similar to C++ class TLVMi::BuffersManager but it doesn't have exact the
    same behaviour.
    """

    def __init__(self, types):
        self.buffers = [bytearray() for i in range(types)]

    def push_byte(self, index, byte):
        if len(self.buffers) > index:
            self.buffers[index].append(byte)

    def get_byte(self, index):
        byte = None

        if len(self.buffers) > index:
            if self.buffers[index]:
                byte = self.buffers[index].pop(0)

        return byte


class TLVDispatcher:
    """Simple TLV dispatcher. Call the corresponding callback, if a TLV has arrived.

    Similar to C++ class TLVMi::TLVDispatcher.
    """

    def __init__(self, tlv_manager):
        self.tlv_manager = tlv_manager
        self.callbacks = {i:None for i in range(len(tlv_manager.jobs_lists))}

    def subscribe_callback(self, tlv_type, callback):
        if tlv_type in self.callbacks:
            self.callbacks[tlv_type] = callback

    def verify_and_distpatch(self):
        tlv_type = 0
        while tlv_type < len(self.callbacks):
            tlv = self.tlv_manager.get_pending_tlv(tlv_type)
            if tlv != None:
                if self.callbacks[tlv_type] != None:
                    self.callbacks[tlv_type](tlv)
                else:
                    print("No callback registered for TLV type: ", tlv_type, "\r")
            else:
                tlv_type += 1
