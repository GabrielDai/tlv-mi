/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once


/**
 * Forward declarations of tlv-mi
 */
namespace TLVMi
{
    template<
        typename data_t,
        std::size_t arrayLength>
    struct Serializable;

    template<typename type_t>
    struct TLV;

    template<
        typename type_t,
        std::size_t sizeOfList,
        std::size_t amountOfLists>
    class TLVManager;

    template<
        std::size_t sizeOfBuffer,
        std::size_t amountOfBuffers>
    class BuffersManager;

    template<
        typename tlv_mgr_t,
        typename buffer_mgr_t,
        typename protocol_t,
        typename consumer_t>
    class EncodeRoutine;

    template<std::size_t buffers>
    class MsgVerifier;

    template<
        typename tlv_mgr_t,
        typename buffer_mgr_t,
        typename protocol_t,
        typename data_receiver_t,
        std::size_t sizeOfBuffer>
    class DecodeRoutine;

    template<
        typename tlv_mgr_t,
        typename type_t>
    class TLVDispatcher;
}
