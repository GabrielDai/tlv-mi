/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <memory>
#include <cstdint>

#include "tlv-mi/TLVMi.h"


/**
 * Generic TLV(Type Length Value) definition.
 */
template<typename type_t>
struct TLVMi::TLV
{
    explicit TLV(type_t type, std::size_t length, std::unique_ptr<uint8_t[]> buffer)
        : type(type),
          length(length),
          buffer(std::move(buffer)) {
    }
    type_t type;
    std::size_t length;
    std::unique_ptr<uint8_t[]> buffer;
};
