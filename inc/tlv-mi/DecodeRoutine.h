/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include <stdio.h>

#include "tlv-mi/TLVMi.h"
#include "tlv-mi/TLV.h"
#include "tlv-mi/MsgVerifier.h"


/**
 * Receive data, decode it into BuffersManager and if all bytes have been
 * received create a TLV, which is pushed into TLVManager.
 *
 * The user should implement the data receiver because this depends on how
 * the data is sent.
 *
 * The created TLVManager can be dispatched by TLVDispatcher.
 */
template<
    typename tlv_mgr_t,
    typename buffer_mgr_t,
    typename protocol_t,
    typename data_receiver_t,
    std::size_t sizeOfBuffer>
class TLVMi::DecodeRoutine
{
    using protocol_decoder_t = typename protocol_t::decoder_t;
    using msg_t = typename tlv_mgr_t::msg_t;

    public:
        explicit DecodeRoutine(
                tlv_mgr_t& tlvManager,
                data_receiver_t& receiver) :
            mTlvManager(tlvManager),
            mDataReceiver(receiver) {
        }

        /**
         * Run the decode routine one time
         */
        void run() {
            // Arrange arrived messages:
            // 1. Get bytes into general buffer
            // 2. Decode bytes from general buffer into buffers according to protocol
            // 3. Create TLV and push it to TLVManager
            // 4. [Other thread] Dispatch TLV (send TLV to the subscribers)

            std::array<uint8_t, sizeOfBuffer> rxBuffer;
            auto length = mDataReceiver.obtainBytes(rxBuffer.begin(), rxBuffer.size());
            if (length > 0) {
                mProtocolDecoder.decode(
                        rxBuffer.begin(),
                        length,
                        [this](std::size_t type, uint8_t byte) {mBufferMgr.pushByte(type, byte);},
                        [this](std::size_t type) {mMsgVerifier.registerCompleteMsg(type);});

                while (mMsgVerifier.someCompleteMsg()) {
                    auto buffer = mMsgVerifier.getBufferWithCompleteMsg();
                    createTLV(buffer);
                }
            }
        }

    private:
        /**
         * Auxiliary function for TLV creation from BuffersManager
         */
        void createTLV(std::size_t buffer) {
            auto type = static_cast<msg_t>(buffer);
            auto length = static_cast<std::size_t>(mBufferMgr.getByte(buffer));

            auto value = std::make_unique<uint8_t[]>(length);
            for (std::size_t i = 0; i < length; i++) {
                value[i] = mBufferMgr.getByte(buffer);
            }

            auto tlv = std::make_unique<TLV<msg_t>>(type, length, std::move(value));
            if(!mTlvManager.pushTLV(std::move(tlv))) {
                printf("[Decoding] Producing data faster than consuming\n");
            }
        }

        tlv_mgr_t& mTlvManager;
        data_receiver_t& mDataReceiver;
        protocol_decoder_t mProtocolDecoder;
        buffer_mgr_t mBufferMgr;
        MsgVerifier<buffer_mgr_t::buffers> mMsgVerifier;
};
