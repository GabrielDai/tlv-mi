/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <cstdint>

#include "tlv-mi/TLVMi.h"


/**
 * It contains buffers and controls access to them. These buffers are used to
 * save the representations in bytes of encoded types according to a protocol.
 * User must define this protocol.
 */
template<
    std::size_t sizeOfBuffer,
    std::size_t amountOfBuffers>
class TLVMi::BuffersManager
{
    public:
        explicit BuffersManager() {
                mLastByte.fill(0);
                mCurrentByte.fill(0);
        }

        /**
         * Returns the free space of a corresponding buffer
         */
        std::size_t getFreeSpace(std::size_t buffer) {
            return mBuffers[buffer].size() - mLastByte[buffer];
        }

        /**
         * Returns the current amount of bytes of a corresponding buffer
         */
        std::size_t getPendingBytes(std::size_t buffer) {
            return mLastByte[buffer] - mCurrentByte[buffer];
        }

        /**
         * Push a new byte into a corresponding buffer
         */
        void pushByte(std::size_t buffer, uint8_t byte) {
            if (mLastByte[buffer] < mBuffers[buffer].size()) {
                mBuffers[buffer][mLastByte[buffer]] = byte;
                mLastByte[buffer]++;
            }
        }

        /**
         * Returns the next byte from a corresponding buffer
         */
        uint8_t getByte(std::size_t buffer) {
            uint8_t byte = 0;
            if (mCurrentByte[buffer] < mLastByte[buffer]) {
                byte = mBuffers[buffer][mCurrentByte[buffer]];
                mCurrentByte[buffer]++;

                if (mCurrentByte[buffer] == mLastByte[buffer]) {
                    mCurrentByte[buffer] = 0;
                    mLastByte[buffer] = 0;
                }
            }

            return byte;
        }

        /**
         * Used to know how many buffers the manager contains
         */
        static constexpr std::size_t buffers = amountOfBuffers;
        static constexpr std::size_t bufferSize = sizeOfBuffer;

    private:
        std::array<
            std::array<uint8_t, sizeOfBuffer>,
            amountOfBuffers> mBuffers;
        std::array<
            std::size_t,
            amountOfBuffers> mCurrentByte, mLastByte;
};
