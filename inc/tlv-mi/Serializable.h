/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstdint>
#include <memory>

#include "tlv-mi/TLVMi.h"


/**
 * Used to transform entities into serialized data.
 */
template<
    typename data_t,
    std::size_t arrayLength>
struct TLVMi::Serializable
{
    explicit Serializable(data_t value_[]) {
        for (std::size_t i = 0; i < arrayLength; i++) {
            value[i] = value_[i];
        }
    }

    explicit Serializable(std::unique_ptr<uint8_t[]> buffer) {
        for (std::size_t i = 0; i < bufferLength; i++) {
            bytes[i] = buffer[i] ;
        }
    }

    std::unique_ptr<uint8_t[]> serializeIntoBuffer() {
        auto buffer = std::make_unique<uint8_t[]>(bufferLength);

        for (std::size_t i = 0; i < bufferLength; i++) {
            buffer[i] = bytes[i];
        }

        return std::move(buffer);
    }

    static constexpr std::size_t bufferLength = sizeof(data_t) * arrayLength;

    union {
        data_t value[arrayLength];
        uint8_t bytes[bufferLength];
    };
};
