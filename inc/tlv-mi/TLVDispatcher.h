/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <stdio.h>
#include <functional>
#include <mutex>

#include "tlv-mi/TLVMi.h"
#include "tlv-mi/TLV.h"


/**
 * Simple TLV dispatcher. Call the corresponding callback, if a TLV has arrived.
 */
template<
    typename tlv_mgr_t,
    typename type_t>
class TLVMi::TLVDispatcher
{
    public:
        using tlv_t = std::unique_ptr<TLV<type_t>>;
        using msg_t = typename tlv_mgr_t::msg_t;

        explicit TLVDispatcher(tlv_mgr_t& tlvManager) :
            mTlvManager(tlvManager) {
                mCallbacks.fill(nullptr);
        }

        /**
         * Subscribe a callback to a message type. There can be only one
         * callback per type
         */
        void subscribeCallback(msg_t type, std::function<void(tlv_t)> callback) {
            std::lock_guard<std::mutex> lock(mAccess);
            mCallbacks[static_cast<std::size_t>(type)] = callback;
        }

        /**
         * Check, if a TLV has been received and try to call a callback. This
         * function should be called periodically
         */
        void verifyAndDispatch() {
            std::size_t tlvType = 0;
            while (tlvType < tlv_mgr_t::tlvs) {
                auto tlv = mTlvManager.getPendingTLV(static_cast<msg_t>(tlvType));
                if (tlv != nullptr) {
                    if (mCallbacks[tlvType] != nullptr) {
                        mCallbacks[tlvType](std::move(tlv));
                    } else {
                        printf("No callback registered for TLV type: %u\n", static_cast<unsigned int>(tlvType)); // std::size_t depends on the platform, could be unsigned int or long unsigned int
                    }
                } else {
                    tlvType++;
                }
            }
        }

    private:
        tlv_mgr_t& mTlvManager;
        std::array<
            std::function<void(tlv_t)>,
            tlv_mgr_t::tlvs> mCallbacks;
        std::mutex mAccess;
};
