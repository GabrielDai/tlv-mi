/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <cstddef>

#include "tlv-mi/TLVMi.h"


/**
 * Extract data from TLVManager, copy them into BuffersManager and then call
 * the consumer.
 *
 * The consumer needs to be implemented by the user. The user is who decide
 * how the bytes are consumed (e.g. writing them into a file, sending them
 * through a serial port or a socket).
 */
template<
    typename tlv_mgr_t,
    typename buffer_mgr_t,
    typename protocol_t,
    typename consumer_t>
class TLVMi::EncodeRoutine
{
    using msg_t = typename tlv_mgr_t::msg_t;

    public:
        explicit EncodeRoutine(
                tlv_mgr_t& tlvManager,
                consumer_t& consumer) :
            mTlvManager(tlvManager),
            mConsumer(consumer) {
        }

        /**
         * Run the encode routine one time. It is up to the consumer if it
         * deliver everything from the BuffersManager in one call of this
         * function
         */
        void run() {
            // Move data from TLVManager to BuffersManager:
            // 1. Check space in BuffersManager
            // 2. Subtract space needed by the protocol
            // 3. Get TLV
            // 4. Encapsulate data according to protocol
            // 5. Save into the corresponding buffer

            std::size_t buffer = 0;
            while (buffer < buffer_mgr_t::buffers) {
                auto freeSpace = mBufferMgr.getFreeSpace(buffer);
                freeSpace -= protocol_t::amountOfExtraBytes;

                auto tlv = mTlvManager.getPendingTLV(static_cast<msg_t>(buffer), freeSpace);
                if (tlv != nullptr) {
                    protocol_t::encode(
                            [this, buffer](uint8_t byte) {mBufferMgr.pushByte(buffer, byte);},
                            std::move(tlv));
                } else {
                    buffer++;
                }
            }

            mConsumer.deliverBytes(mBufferMgr);
        }

    private:
        tlv_mgr_t& mTlvManager;
        consumer_t& mConsumer;
        buffer_mgr_t mBufferMgr;
};
