/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <mutex>
#include <cstdint>
#include <limits>

#include "tlv-mi/TLVMi.h"
#include "tlv-mi/TLV.h"


/**
 * It contains buffers for all desired TLV's types and controls the access to
 * them.
 *
 * An unprocessed TLV is called also as pending job.
 */
template<
    typename type_t,
    std::size_t sizeOfList,
    std::size_t amountOfLists>
class TLVMi::TLVManager
{
    public:
        using tlv_t = std::unique_ptr<TLV<type_t>>;
        using msg_t = type_t;

        explicit TLVManager() {
            mLastItems.fill({0,0});
        }

        /**
         * Insert a new TLV into the corresponding buffer
         *
         * \return true if operation was success
         * \return false otherwise
         */
        bool pushTLV(tlv_t tlv) {
            bool jobRegistred = false;
            std::size_t index = static_cast<std::size_t>(tlv->type);
            std::lock_guard<std::mutex> lock(mAccessLists[index]);

            // Next free position
            auto nextWrItem = nextItem(mLastItems[index].wrItem);
            if (nextWrItem != mLastItems[index].rdItem) {
                mJobsLists[index][nextWrItem] = std::move(tlv);
                mLastItems[index].wrItem = nextWrItem;
                jobRegistred = true;
            }

            return jobRegistred;
        }

        /**
         * Return an unique pointer to an unprocessed TLV from a corresponding
         * buffer
         */
        tlv_t getPendingTLV(type_t type, std::size_t size = std::numeric_limits<std::size_t>::max()) {
            tlv_t pendingJob = nullptr;
            std::size_t index = static_cast<std::size_t>(type);

            // Next pending job
            auto nextRdItem = nextItem(mLastItems[index].rdItem);
            if (nextRdItem != nextItem(mLastItems[index].wrItem)) {
                if (mJobsLists[index][nextRdItem]->length <= size) {
                    pendingJob = std::move(mJobsLists[index][nextRdItem]);
                    mLastItems[index].rdItem = nextRdItem;
                }
            }

            return pendingJob;
        }

        static constexpr std::size_t tlvs = amountOfLists;

    private:
        static std::size_t nextItem(std::size_t currentItem) {
            auto nextItem = currentItem + 1;
            if (nextItem == sizeOfList) {
                nextItem = 0;
            }

            return nextItem;
        }

        std::array<
            std::array<tlv_t, sizeOfList>,
            amountOfLists> mJobsLists;
        std::array<std::mutex, amountOfLists> mAccessLists;

        struct WrRdItem {
            std::size_t wrItem;
            std::size_t rdItem;
        };
        std::array<
            WrRdItem,
            amountOfLists> mLastItems;
};
