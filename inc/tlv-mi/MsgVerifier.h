/*
 * SPDX-FileCopyrightText: © 2020 Gabriel Moyano <vgmoyano@gmail.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <array>
#include <cstddef>
#include <cstdint>

#include "tlv-mi/TLVMi.h"


/**
 * Auxiliary class used by DecodeRoutine to know, if all bytes of a TLV have
 * been received.
 */
template<std::size_t buffers>
class TLVMi::MsgVerifier
{
    public:
        explicit MsgVerifier() {
            mWaitingMsgs.fill(0);
        }

        /**
         * Register TLV of type as complete. This function is used indirectly
         * by the decoder
         */
        void registerCompleteMsg(std::size_t type) {
            mWaitingMsgs[type]++;
        }

        /**
         * Returns true, if at least a TLV has been received completely,
         * otherwise false
         */
        bool someCompleteMsg() {
            bool anyReceivedMsg = false;
            for (std::size_t i = 0; i < mWaitingMsgs.size(); i++) {
                if(mWaitingMsgs[i] != 0) {
                    anyReceivedMsg = true;
                    mBufferWithMsg = i;
                    break;
                }
            }
            return anyReceivedMsg;
        }

        /**
         * Returns the buffer index where a complete message was found
         */
        std::size_t getBufferWithCompleteMsg() {
            mWaitingMsgs[mBufferWithMsg]--;
            return mBufferWithMsg;
        }

    private:
        std::array<uint16_t, buffers> mWaitingMsgs;
        std::size_t mBufferWithMsg;
};
